package io.mldelaney.bitbucket.checks;

import com.atlassian.bitbucket.hook.repository.PreRepositoryHookContext;
import com.atlassian.bitbucket.hook.repository.PullRequestMergeHookRequest;
import com.atlassian.bitbucket.hook.repository.RepositoryHookResult;
import com.atlassian.bitbucket.hook.repository.RepositoryMergeCheck;
import com.atlassian.bitbucket.i18n.I18nService;

import com.atlassian.plugin.spring.scanner.annotation.imports.BitbucketImport;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;

import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.atlassian.sal.api.net.Request;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import io.mldelaney.bitbucket.utils.GitRef;

/**
 *
 * @author mdelaney
 *
 */
public class JiraFixVersionCheck implements RepositoryMergeCheck {

  /**
  * Our logger for BitBucket Server
  */
  private static final Logger log = LoggerFactory.getLogger(JiraFixVersionCheck.class);

  /**
   * Expression to find our branch name, the group break down is:
   *    1) Branch folder
   *    2) JIRA Issue
   *    3) JIRA Project key
   *    4) JIRA Project index
   *    5) Remaining branch data.
   */
  private static final String REGEX = "^refs\\/heads\\/([A-Za-z0-9]+)\\/(([A-Za-z][A-Za-z0-9]+)-([0-9]+))-(.*)$";

  /**
   * Internationalization service.
   */
  private final I18nService i18nService;

  /**
   * Atlassian Application link lookup service
   */
  private final ApplicationLinkService appLinkService;

  /**
   * Internal link to Atlassian JIRA.
   */
  protected ApplicationLink appLink;

  /**
   *
   */
  protected ApplicationLinkRequestFactory applicationLinkRequestFactory;

  /**
   *
   */
  protected ApplicationLinkResponseHandler<JsonObject> handler;

  /**
   *
   * @param applicationLinkService
   * @param i18nService
   */
  public JiraFixVersionCheck(@BitbucketImport ApplicationLinkService applicationLinkService, @BitbucketImport I18nService i18nService) {
    this.i18nService = i18nService;
    this.appLinkService = applicationLinkService;

    setupConnectionLink();

    handler = new ApplicationLinkResponseHandler<JsonObject>() {
      @Override
      public JsonObject handle(Response response) throws ResponseException {
        JsonParser parser = new JsonParser();

        assert response.getStatusCode() == 200;
        return (JsonObject) parser.parse(response.getResponseBodyAsString());
      }

      @Override
      public JsonObject credentialsRequired(Response response) throws ResponseException {
        return null;
      }
    };
  }

  /**
   *
   */
  protected void setupConnectionLink() {
	if (appLink != null) {
	  log.info(String.format("Link %s (%s) already active, no need to re-create the link",
			   				 appLink.getName(),
			   				 appLink.getDisplayUrl()));
	  return;
	}

    try {
      appLink = appLinkService.getPrimaryApplicationLink(JiraApplicationType.class);
      if (applicationLinkRequestFactory == null) {
        applicationLinkRequestFactory = appLink.createAuthenticatedRequestFactory();
      }
    }
    catch (NullPointerException ex) {
      log.error("Unable to fine a JIRA Application link", ex);
    }
  }


  @Override
  public RepositoryHookResult preUpdate(PreRepositoryHookContext arg0, PullRequestMergeHookRequest arg1) {
    setupConnectionLink();

    final String branch = GitRef.branchName(arg1.getToRef().getId());

    /** Only check when we are going to master, or release branch */
    if (!GitRef.isProtectedBranch(branch)) {
      return RepositoryHookResult.accepted();
    }

    final String fromBranch = arg1.getPullRequest().getFromRef().getId();
    Pattern p = Pattern.compile(REGEX);
    Matcher matches = p.matcher(fromBranch);

    if (matches.matches()) {
      final String key = matches.group(2);
      final String url = String.format("rest/api/latest/issue/%s", key);

      try {
        ApplicationLinkRequest request = applicationLinkRequestFactory.createRequest(Request.MethodType.GET, url);
        JsonObject response = request.execute(handler);

        JsonObject fields = response.get("fields").getAsJsonObject();
        JsonArray versions = fields.get("fixVersions").getAsJsonArray();

        if (versions.size() == 0) {
          return RepositoryHookResult.rejected(
              i18nService.getMessage("io.mldelaney.bitbucket.checks.jirafixversion.error.title"),
              i18nService.getMessage("io.mldelaney.bitbucket.checks.jirafixversion.error.message", key));
        }
      }
      catch (CredentialsRequiredException ex) {
        log.error("Error with Credentials", ex);
      }
      catch (NullPointerException ex) {
        log.error(
            String.format("Unable to find JIRA Issue %s", key),
            ex);
      }
      catch (Exception ex) {
        log.error("Opps, soemthing went wrong", ex);
      }
    }

    return RepositoryHookResult.accepted();
  }
}
