package io.mldelaney.bitbucket.utils;

import org.apache.commons.io.FilenameUtils;

public final class GitRef {

  protected static final String REFS_HEADS = "refs/heads/";
  protected static final String REFS_TAGS = "refs/tags/";

  /**
   * Removes the Git reference prefix from branch.
   *
   * @param ref
   * @return
   */
  public static String branchName(final String ref) {
    String result = ref;

    if (ref.startsWith(REFS_HEADS)) {
      result = ref.replace(REFS_HEADS, "");
    }
    else if (ref.startsWith(REFS_TAGS)) {
      result = ref.replace(REFS_TAGS, "");
    }

    return result;
  }

  /**
   * Checks to see if a branch matches a directory structure.
   *
   * @param ref
   * @return
   */
  public static boolean isProtectedBranch(final String ref) {
    String[] branches = {"master", "release/*"};
    for (String branch: branches) {
      if (FilenameUtils.wildcardMatch(ref, branch)) {
        return true;
      }
    }

    return false;
  }
}
