package io.mldelaney.bitbucket.utils;

import java.util.Map;

import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;

/**
 *
 */
public class JsonUtils {
  /**
   * Parse a JSON String and convert into a HashMap.
   *
   * @param jsonString
   * @return
   * @throws JsonIOException
   */
  public static Map<String, Object> jsonString2Map( String jsonString ) throws JsonIOException {
    JsonParser parser = new JsonParser();
    JsonObject json = (JsonObject) parser.parse(jsonString);

    Type type = new TypeToken<Map<String, Object>>(){}.getType();
    Map<String, Object> map = new Gson().fromJson(json, type);

    return map;
  }
}
