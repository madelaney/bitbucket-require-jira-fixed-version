package ut.io.mldelaney.bitbucket;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import io.mldelaney.bitbucket.utils.GitRef;

public class GitRefTest {

  @Test
  public void testIsProtectedBranch() {
    assertTrue(GitRef.isProtectedBranch("master"));
    assertTrue(GitRef.isProtectedBranch("release/0.1.0"));
    assertFalse(GitRef.isProtectedBranch("feature/fubar"));
  }

  @Test
  public void testExtractTagName() {
    assertEquals("v1.0.0", GitRef.branchName("refs/tags/v1.0.0"));
  }

  @Test
  public void testBranchExtraction() {
    assertEquals("master", GitRef.branchName("refs/heads/master"));
  }
}
